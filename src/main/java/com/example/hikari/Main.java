package com.example.hikari;

import com.example.hikari.sql.SQLManager;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {

    private SQLManager sqlManager;

    @Override
    public void onEnable() {
        loadDatabase();
    }

    @Override
    public void onDisable() {
        sqlManager.onDisable();
    }

    private void loadDatabase() {
        sqlManager = new SQLManager(this);
    }

    public SQLManager getSQLManager() {
        return sqlManager;
    }

}
