package com.example.hikari.sql;

import com.example.hikari.Main;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class SQLManager {

    private final Main plugin;
    private final ConnectionPoolManager poolManager;

    public SQLManager(Main plugin) {
        this.plugin = plugin;
        poolManager = new ConnectionPoolManager(plugin);
    }

    public void exampleQuery() {
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet res = null;
        try {
            conn = poolManager.getConnection();
            ps = conn.prepareStatement("SELECT * FROM `ExampleTable` WHERE ExampleField='ExampleValue';");
            res = ps.executeQuery();
            // ResultSet now filled with example query return
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            poolManager.close(conn, ps, res);
        }
    }

    public void onDisable() {
        poolManager.closePool();
    }

}
