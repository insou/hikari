package com.example.hikari.sql;

import com.example.hikari.Main;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ConnectionPoolManager {

    private final Main plugin;

    private HikariDataSource dataSource;

    private String hostname;
    private String port;
    private String database;
    private String username;
    private String password;

    private int minConnections;
    private int maxConnections;
    private long timeout;

    public ConnectionPoolManager(Main plugin) {
        this.plugin = plugin;
        init();
        setupPool();
    }

    private void init() {
        hostname = plugin.getConfig().getString("sql.hostname");
        port = plugin.getConfig().getString("sql.port");
        database = plugin.getConfig().getString("sql.database");
        username = plugin.getConfig().getString("sql.username");
        password = plugin.getConfig().getString("sql.password");

        minConnections = plugin.getConfig().getInt("sql.pool.minimum-connections");
        maxConnections = plugin.getConfig().getInt("sql.pool.maximum-connections");
        timeout = plugin.getConfig().getLong("sql.pool.connection-timeout");
    }

    private void setupPool() {
        HikariConfig config = new HikariConfig();

        config.setJdbcUrl("jdbc:mysql://" + hostname + ":" + port + "/" + database);
        config.setDriverClassName("com.mysql.jdbc.Driver");
        config.setUsername(username);
        config.setPassword(password);

        config.setMinimumIdle(minConnections);
        config.setMaximumPoolSize(maxConnections);
        config.setConnectionTimeout(timeout);

        dataSource = new HikariDataSource(config);
    }

    public Connection getConnection() throws SQLException {
        return dataSource.getConnection();
    }

    public void close(Connection conn, PreparedStatement ps, ResultSet res) {
        if (conn != null) { try { conn.close(); } catch (SQLException e) { e.printStackTrace(); } }
        if (ps != null) { try { ps.close(); } catch (SQLException e) { e.printStackTrace(); } }
        if (res != null) { try { res.close(); } catch (SQLException e) { e.printStackTrace(); } }
    }

    public void closePool() {
        if (dataSource != null && !dataSource.isClosed()) {
            dataSource.close();
        }
    }

}
